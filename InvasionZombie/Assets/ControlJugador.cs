using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlJugador : MonoBehaviour
{

    public float rapidezDesplazamiento = 10.0f;
    public GameObject proyectilPrefab;
    public Transform puntoDisparo;

    public int limiteBalas = 100;
    public KeyCode teclaRecarga = KeyCode.R;
    private int balasRestantes;
    private float tiempoUltimoDisparo;


    public GameObject Pistola;
    public float fuerzaDeDisparo = 10f;
    //public float retrasoEntreDisparos = 0.5f;
    public float velocidadDisparo = 5f;
    public float velocidadProyectil = 10f;




    private bool metralletaActivada = false;
    private bool disparando = false;

    public GameObject metralleta;

    public Camera camaraPrimeraPersona;
    // Start is called before the first frame update
    void Start()
    {
        balasRestantes = limiteBalas;

        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;

        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;

        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);

        if (Input.GetKeyDown("escape")) 
        {
            Cursor.lockState = CursorLockMode.None;
        }


       
        if (Input.GetKeyDown(KeyCode.Alpha1))
        {
            metralletaActivada = false;
            Pistola.SetActive(true);
            metralleta.SetActive(false);
            DetenerDisparo();
        }
        else if (Input.GetKeyDown(KeyCode.Alpha2))
        {
            metralletaActivada = true;
            Pistola.SetActive(false);
            metralleta.SetActive(true);
            if (Input.GetMouseButton(0))
            {
                IniciarDisparo();
            }
        }

        if (metralletaActivada && Input.GetMouseButtonDown(0))
        {
            IniciarDisparo();
        }
        else if (Input.GetMouseButtonUp(0))
        {
            DetenerDisparo();
        }

        if (Input.GetKeyDown(teclaRecarga))
        {
            Recargar();
        }
    }

    void IniciarDisparo()
    {
        if (!disparando && balasRestantes > 0)
        {
            disparando = true;
            Disparar();
        }
    }

    void DetenerDisparo()
    {
        if (disparando)
        {
            disparando = false;
        }
    }

    void Disparar()
    {
        if (balasRestantes > 0)
        {
            if (Time.time - tiempoUltimoDisparo >= velocidadDisparo)
            {
                GameObject pro = Instantiate(proyectilPrefab, puntoDisparo.position, puntoDisparo.rotation);
                Rigidbody rb = pro.GetComponent<Rigidbody>();
                rb.AddForce(puntoDisparo.forward * fuerzaDeDisparo, ForceMode.Impulse);
                rb.velocity = puntoDisparo.forward * velocidadProyectil;
                Destroy(pro, 5f);
                balasRestantes--;
                tiempoUltimoDisparo = Time.time;
            }
        }

        if (disparando)
        {
            Invoke("Disparar", velocidadDisparo);
        }
    }

    void Recargar()
    {
        balasRestantes = limiteBalas;
    }

}
